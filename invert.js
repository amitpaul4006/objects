function invert(testObject) {
    const keys = Object.keys(testObject);
    const values = Object.values(testObject);
    const myMap = {};
    for (let i = 0; i < keys.length; i++) {

        myMap[values[i]] = keys[i];
    }
    return myMap;
}
module.exports = invert;