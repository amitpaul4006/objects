function defaults_fun(testObject, New_obj) {
    for (let key in New_obj) {
        if (key in testObject === false) {
            testObject[key] = New_obj[key];
        }
    }
    return testObject
}
module.exports = defaults_fun;