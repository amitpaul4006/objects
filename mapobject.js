function MapObject(testObject, callBack) {
    const result_obj = {};
    for (let i in testObject) {
        result_obj[i] = callBack(testObject[i]);
    }
    return result_obj;



}
module.exports = MapObject;